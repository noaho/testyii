<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
//use yii\behaviors\BlameableBehavior;
//use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }
		public function getCategoryItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
	
	
		public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }
	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
	public function beforeSave($insert){//צריך להוסיף- יצירת חוג חדש נשמר בסטטוס מסוים
		$return = parent::beforeSave($insert);
		if($this->isNewRecord){
			$this->setAttribute('statusId','2');
		}
		return $return;
	}
	
}
