<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
   public $role;//מיוחדת כי היא לא באה מטבלת יוזר- הרול נשמר בטבלת auth-assinment
     public static function tableName(){//מבוססת על הטבלה של USER
		return 'user';
	}
	


	public function rules(){//ולידציה
		return
		[ 
			[['name','username', 'password', 'auth_Key'], 'string', 'max' =>255],
			[['username', 'password'], 'required'],
			[['username'], 'unique'],
			['role', 'safe'],
		];
	}

 public static function findIdentity($id)//יודע ללכת לטבלת USER 
    {
		
		return self::findOne($id);//מצא את הת.ז בטבלה במסד נתונים
		
      
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not Supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_Key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
//create fullname pseuodo field
	/*public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }*/
	
	//A method to get an array of all users models/User
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $users;						
	}	
	
	public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}	
	
		public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }
}


