<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170716_171039_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->createTable(
            'activity',
            [
                'id' => 'pk',
                'title' => 'string',	
				'categoryId'=>'integer',
				'statusId'=>'integer',
						
            ],
            'ENGINE=InnoDB'
        );
    }
        

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}
