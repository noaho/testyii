<?php

use yii\db\Migration;

/**
 * Handles adding CategoryId to table `user`.
 */
class m170716_170322_add_CategoryId_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'CategoryId', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'CategoryId');
    }
}
