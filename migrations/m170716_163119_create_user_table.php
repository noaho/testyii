<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170716_163119_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'name' => $this->String()->notNull(),
			'username' => $this->String()->notNull(),
			'password' => $this->String()->notNull(),
			'auth_Key' => $this->String()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
