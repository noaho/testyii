<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'auth_Key')->textInput(['maxlength' => true]) ?>
	<?= $model->isNewRecord ? '' : $form->field($model, 'CategoryId')->dropDownList(Category::getCategorys()) ?>
	
	<?//= $model->isNewRecord ? '' : $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	
	<?php if (!$model->isNewRecord) { ?>
		<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	<?php } else { ?>
		<div style="display:none;"><?= $model->isNewRecord ? $form->field($model, 'role')->textInput(['value'=>simpleUser]) : ''?></div>
	<?php } ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
