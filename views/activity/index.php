<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'categoryId',
            //'statusId',//יש להוסיף על מנת שיופיע השם של הסטטוס וקטגוריה בעמוד של החוגים
			[
				'attribute' => 'catagoryId',
				'label' => 'Catagory Name',
				'value' => function($model){
					return $model->categoryItem->name;
					},
				'filter'=>Html::dropDownList('ActivitySearch[categoryId]', 
				$categoryId, $categorys, ['class'=>'form-control']),	
			],
            [
				'attribute' => 'statusId',
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->name;
					},		
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
